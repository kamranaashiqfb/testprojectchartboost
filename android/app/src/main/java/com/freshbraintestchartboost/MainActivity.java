package com.freshbraintestchartboost;
import com.RNChartboost.RNChartboostPackage;
import android.os.Bundle;
import com.facebook.react.ReactActivity;

public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "freshbraintestchartboost";
  }




  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    RNChartboostPackage.ONCREATE(this, "5f4e1cce6b9ae60909e6cfae", "37de3a1808db850866dbe8b416c9e3b1c4b9071f");
  }

  @Override
  public void onStart() {
    super.onStart();
    RNChartboostPackage.ONSTART(this);
  }

  @Override
  public void onResume() {
    super.onResume();
    RNChartboostPackage.ONRESUME(this);
  }

  @Override
  public void onPause() {
    super.onPause();
    RNChartboostPackage.ONPAUSE(this);
  }

  @Override
  public void onStop() {
    super.onStop();
    RNChartboostPackage.ONSTOP(this);
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    RNChartboostPackage.ONDESTROY(this);
  }

  @Override
  public void onBackPressed() {
    if (RNChartboostPackage.ONBACKPRESSED())
      return;
    else
      super.onBackPressed();
  }
}
